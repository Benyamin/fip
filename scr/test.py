# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 18:42:18 2017

@author: Benyamin
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from sklearn.preprocessing import Normalizer
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score

matplotlib.style.use('ggplot') # Look Pretty
df = pd.read_csv('C:\\Users\\Benyamin\\Desktop\\project\\testdata.csv')
df.Hp = (df['1_Cauchy']+df['5_Cauchy']+df['9_Cauchy'])/3
y = df['totalshear']
X = df.drop(['totalshear'], axis=1)

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
data_train, data_test, label_train, label_test = train_test_split(X, y, test_size=0.2, random_state=1)

n = Normalizer().fit(data_train)
norm_train = n.transform(data_train);
norm_test = n.transform(data_test);

reg = linear_model.LinearRegression()
reg.fit (norm_train, label_train)


y_pred = reg.predict(norm_test)

# The coefficients
print('Coefficients: \n', reg.coef_)
# The mean squared error
print("Mean squared error: %.2f"
      % mean_squared_error(label_test, y_pred))
# Explained variance score: 1 is perfect prediction
print('R^2 score: %.2f' % r2_score(label_test, y_pred))

import statsmodels.formula.api as sm
result = sm.ols(formula=label_train~data_train, data=df).fit()
#print result.summary()
print result.rsquared, result.rsquared_adj