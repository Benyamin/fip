# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 14:38:41 2017

@author: Benyamin
"""
import numpy as np
#import math
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
#import mpl_toolkits.mplot3d as Axes3d
#from sklearn.preprocessing import Normalizer
#from sklearn import datasets, linear_model
#from sklearn.metrics import mean_squared_error, r2_score
import time
matplotlib.style.use('ggplot') # Look Pretty
from lattice import slipNormal 
def slipPlanes(crystal):
    # Slip plane normals in Miller indices.
    if crystal == 'HCP':
        sp= np.array(  [[      0,      0,   1.0],
                        [      0,      0,   1.0],
                        [      0,      0,   1.0],
                        
                        [      0,    1.0,     0],
                        [ -0.866,   -0.5,     0],
                        [  0.866,   -0.5,     0],
                        
                        [      0,  0.738, 0.675],
                        [ -0.639, -0.369, 0.675],
                        [  0.639, -0.369, 0.675],
                        [ -0.639,  0.369, 0.675],
                        [      0, -0.738, 0.675],
                        [  0.639,  0.369, 0.675],
                        
                        [ -0.639,  0.369, 0.675],
                        [ -0.639,  0.369, 0.675],
                        [  0.639,  0.369, 0.675],
                        [  0.639,  0.369, 0.675],
                        [      0, -0.738, 0.675],
                        [      0, -0.738, 0.675],
                        [  0.639, -0.369, 0.675],
                        [  0.639, -0.369, 0.675],
                        [ -0.639, -0.369, 0.675],
                        [ -0.639, -0.369, 0.675],
                        [      0,  0.738, 0.675],
                        [      0,  0.738, 0.675],
                        
                        [ -0.687,      0, 0.726],
                        [  0.344, -0.595, 0.726],
                        [  0.344,  0.595, 0.726],
                        [  0.687,      0, 0.726],
                        [ -0.344,  0.595, 0.726],
                        [ -0.344, -0.595, 0.726]])
        names = {'Basal': 3, 'Prismatic':6, 'Pyramidal I <a>':12,
                 'Pyramidal I <c+a>':24, 'Pyramidal II <c+a>':30}
        sp = np.reshape(sp, (30, 3, 1))
    elif crystal == 'BCC':
        sp = np.zeros((2,2))
    elif crystal == 'FCC':
        #TODO 
        sp = np.zeros((2,2))   
    else:
        raise ValueError("Unkown crystal: "+crystal)
    return sp, names
    

class calculateFIP:
    def __init__(self, fname, fname_min, crystal, yieldstress):
        self.df     = pd.read_csv(fname)
        self.df_old = pd.read_csv(fname_min)
        self.slipPlanes, self.slipNames = slipPlanes(crystal)
#        self.slipPlanes  = slipNormal('hex', 1.585)
        self.yieldstress = yieldstress
        for i in range(1, len(self.slipPlanes)+1):
            self.df.rename(columns={str(i)+'_accumulatedshear_slip': 
                                                'slip_'+str(i)}, inplace=True)
            self.df.rename(columns={str(i)+'_resolvedstress_slip':
                                                 'rss_'+str(i)}, inplace=True)
            self.df_old.rename(columns={str(i)+'_accumulatedshear_slip': 
                                                'slip_'+str(i)}, inplace=True)
            self.df_old.rename(columns={str(i)+'_resolvedstress_slip':
                                                 'rss_'+str(i)}, inplace=True)
            
            self.df['dslip_'+str(i)] = self.df['slip_'+str(i)] - \
                                                    self.df_old['slip_'+str(i)]
        self.df.drop    (['phase', 'Cell Type'], axis=1, inplace=True)
        self.df_old.drop(['phase', 'Cell Type'], axis=1, inplace=True)

    def FIP_FS(self):
        df2 = self.df[['SXX', 'SXY', 'SXZ',
                       'SXY', 'SYY', 'SYZ',
                       'SXZ', 'SYZ', 'SZZ']]
        s = df2.as_matrix()
        sigma = s.reshape((s.shape[0], 3, 3))
        
        df3 = self.df[['phi1', 'Phi', 'phi2']]
        E = np.deg2rad(df3.as_matrix())
        Cos = np.cos(E)
        Sin = np.sin(E)
        # convert to passive rotation 
        R00 =  np.multiply(Cos[:,0], Cos[:,2]) - \
                        np.multiply(np.multiply(Sin[:,0], Sin[:,2]), Cos[:,1])
        R10 = -np.multiply(Cos[:,0], Sin[:,2]) - \
                        np.multiply(np.multiply(Sin[:,0], Cos[:,2]), Cos[:,1])
        R20 =  np.multiply(Sin[:,0], Sin[:,1])
        R01 =  np.multiply(Sin[:,0], Cos[:,2]) + \
                        np.multiply(np.multiply(Cos[:,0], Sin[:,2]), Cos[:,1])
        R11 = -np.multiply(Sin[:,0], Sin[:,2]) + \
                        np.multiply(np.multiply(Cos[:,0], Cos[:,2]), Cos[:,1])
        R21 = -np.multiply(Cos[:,0], Sin[:,1])
        R02 =  np.multiply(Sin[:,2], Sin[:,1])
        R12 =  np.multiply(Cos[:,2], Sin[:,1])
        R22 =  Cos[:,1]
        Rm  = np.array([R00, R01, R02, R10, R11, R12, R20, R21, R22]).T

        Rr = Rm.reshape((Rm.shape[0], 3, 3))

        sigma_crystal = Rr*sigma*Rr.transpose((0, 2 ,1))
        #plt.figure(1)
        #plt.hist(sigma_crystal[:,0,2], bins=50)
        #plt.title("Local stress 02")
        
        Tn = np.einsum('sji, njk->sink', self.slipPlanes, sigma_crystal) 
        print Tn.shape
        Tn = np.einsum('sink, ski->ns', Tn, self.slipPlanes) 
        
        print Tn.shape
        #plt.figure(5)
        #plt.plot(Tn[:,15], '*')
        #plt.title("Normal stress component")
        dslip = self.df.loc[:, self.df.columns.str.startswith('dslip')]

        FS =  0.5*np.multiply(dslip,  1+0.5*Tn/self.yieldstress)
        FS.rename(columns=lambda x: x.replace('dslip', 'FIP_FS'), inplace=True)
        self.df[FS.columns] = FS
        
        
        self.df['FIP_total'] = \
        self.df.loc[:, self.df.columns.str.startswith('FIP_FS')].sum(axis=1)
        
        #FS[['dslip_12','dslip_5','dslip_6']].hist(bins=50)
        A=FS[['FIP_FS_'+str(i) for i in range(16,25)]]
#        A.filter(A<1e-3).hist(bins=50)
        
        #self.plothistogram(A)
#        A.plot.kde()
        axes = plt.gca()
        axes.set_xlim([0,0.01])
        #FS["Max"] = FS['FIP_FS_4'] > 4e-3
        #print FS
        #B = FS["Max"].values.reshape((32,32,32)) 
        #x=y=z= np.linspace(0, 31, 32)
        #xv, yv, zv = np.meshgrid(x,y,z)
        #fig = plt.figure()
        #ax = fig.add_subplot(111, projection='3d')
        #ax.scatter(xv[B], yv[B], zv[B])
        #axes.set_ylim([ymin,ymax])
        #ax.set_xlabel("FIP")
        #ax.set_ylabel("Frequency")
       # ax1.set_xlim(0, 0.012)
        #ax1.set_ylim(0, 4000)
        #plt.figure(4)
       # for i in range(0,30)
       #     plt.hist(FS.values[:,i],bins=50)
        return FS
    
    def FIP_MPS(self):
        MPS = 0.5*self.df.loc[:, self.df.columns.str.startswith('dslip')]
        MPS.rename(columns=lambda x: x.replace('dslip', 'FIP_FS'), inplace=True)
        A=MPS[['FIP_FS_'+str(i) for i in range(4,7)]]
        A.hist(bins=50)
        return MPS
    
    def plothistogram(self, df):
        # the histogram of the data
        fig = df['FIP_FS_4'].hist(bins=50)

        fig.set_xlabel('FIP (FS)')
        fig.set_ylabel('Probability')
        fig.set_title('Prismatic slip')
        #fig.set_xlim([0,0.01])
        #plt.axis([40, 160, 0, 0.03])
    
    def plotGrainAverage(self):      
        import seaborn as sns
       
#        sns.set(style="ticks")
#        # Draw a nested boxplot to show bills by day and sex
#        sns.boxplot(x="texture", y="FIP_total", data=self.df, palette="PRGn")
#        sns.despine(offset=10, trim=True)
        
        grain_level = self.df.groupby('texture')['FIP_total'].describe().reset_index()
        g = sns.FacetGrid(grain_level,  margin_titles=True)
        bins = np.linspace(0.0001, 0.005, 50)
        g.map(plt.hist, "mean",  bins=bins, lw=0)
        
def main():
    plt.close("all")
    start_time = time.time()
#    fname     = 'C:\\Users\\Benyamin\\Desktop\\pythonProjects\\FIP\\Notch_end_of_3rdCycle.csv'
#    fname_min = 'C:\\Users\\Benyamin\\Desktop\\pythonProjects\\FIP\\Notch_start_of_3rdCycle.csv'
    
    fname     = 'C:\\Users\\Benyamin\\Desktop\\pythonProjects\\FIP\\New folder\\test.csv'
    fname_min = 'C:\\Users\\Benyamin\\Desktop\\pythonProjects\\FIP\\New folder\\test_old.csv'
    yieldstress = 370e6
    FIP = calculateFIP(fname,fname_min, 'HCP', yieldstress)
    print('Fatemi-Socie FIP:')
    FIP.FIP_FS();
    print('MPS FIP:')
    FIP.FIP_MPS();
    print("--- %s seconds ---" % (time.time() - start_time))
    plt.figure(8)
    FIP.plotGrainAverage()
    pass
    
if __name__ == '__main__':
    main()